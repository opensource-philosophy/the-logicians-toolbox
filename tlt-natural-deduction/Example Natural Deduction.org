#+ATTR_LATEX: :environment natded
| Sterne | Zeile | Formel        | Bezug | Regel |
|--------+-------+---------------+-------+-------|
| +-     |     1 | $p$           | -     | $Hyp$ |
| -+     |     3 | $r$           | -     | $Hyp$ |
| -+     |     2 | $p \lor s$       | 3     | $I\lor$  |
| ++     |     3 | $p \land (p \lor s)$ | 1,2   | $I\land$  |

#+ATTR_LATEX: :environment natded
| Sterne | Zeile | Formel                    | Bezug | Regel           |
|--------+-------+---------------------------+-------+-----------------|
| +--    |     1 | $p \to q$                   | -     | Hyp             |
| -+-    |     2 | $q \to \lnot p$                  | -     | Hyp             |
| +++    |     6 | $\bot$                       | 3,5   | $E\lnot $            |
| --+    |     6 | $p$                       | -     | Hyp             |
| +-+    |     4 | $q$                        | 1,3   | $E\kern-1ex\to$   |
| +++    |     5 | $\lnot p$                      | 2,4   | $E\kern-1ex\to$   |
| ++-    |     7 | $\lnot p$                      | 3,6   | $I\lnot $            |
| ---    |     8 | $(p \to q) \land (q \to \lnot p) \to \lnot p$ | 1,2,7 | $I\kern-1ex\to^+$ |

#+ATTR_LATEX: :environment natded
| Sterne | Zeile | Formel        | Bezug | Regel                         |
|--------+-------+---------------+-------+-------------------------------|
| +   |     1 | $Mx=Nx$       | -     | Hypothesis                    |
| +   |     2 | $λx.Mx=λx.Nx$ | 1     | eqref:xi, $\mathit{x} \notin FV(MN)$ |
| -   |     3 | $λx.Mx=M$     | -     | eqref:eta, $\mathit{x} \notin FV(M)$ |
| -   |     4 | $λx.Nx=N$     | -     | eqref:eta, $\mathit{x} \notin FV(N)$ |
| +   |     5 | $λx.Mx=N$     | 2,4   | eqref:tau                     |
| - |     6 | $M=λx.Mx$     | 3     | eqref:sigma                   |
| -   |     7 | $M=N$         | 6,5 | eqref:tau                     |

Es liegt vermutlich nicht an den Regelzusätzen. Es liegt auch nicht an den Referenzen. Es liegt an der Menge der Sterne!

Idee:  If-Statement: Wenn es nur einen Tern gibt, benutze anderen code für die Sternspalte.

#+BEGIN_EXPORT latex
\begin{center}
\setbox1=\hbox{Sterne}
\begin{tabular}{|>{\centering\arraybackslash}p{\dimexpr\wd1/1}|c|c|c|r|}
\hline
\multicolumn{1}{|c|}{Sterne} & Zeile & Formel & Bezug & Regel \\ \hline
\hline
$\star$ & & 1 & \(Mx=Nx\) & - & Hypothesis \\ \hline
$\star$ & & 2 & \(λx.Mx=λx.Nx\) & 1 & \eqref{xi}, \(\mathit{x} \notin FV(MN)\) \\ \hline
        & & 3 & \(λx.Mx=M\) & - & \eqref{eta}, \(\mathit{x} \notin FV(M)\) \\ \hline
        & & 4 & \(λx.Nx=N\) & - & \eqref{eta}, \(\mathit{x} \notin FV(N)\) \\ \hline
$\star$ & & 5 & \(λx.Mx=N\) & 2,4 & \eqref{tau} \\ \hline
        & & 6 & \(M=λx.Mx\) & 3 & \eqref{sigma} \\ \hline
        & & 7 & \(M=N\) & 6,5 & \eqref{tau} \\ \hline

\end{tabular}
\end{center}
#+END_EXPORT

Idee:
#+BEGIN_EXPORT latex
\begin{center}
\setbox1=\hbox{Sterne}
\begin{tabular}{|>{\centering\arraybackslash}p{\dimexpr\wd1/1}|c|c|c|r|}
\hline
Sterne & Zeile & Formel & Bezug & Regel \\ \hline
\hline
$\star$ & 1 & \(Mx=Nx\) & - & Hypothesis \\ \hline
$\star$ & 2 & \(λx.Mx=λx.Nx\) & 1 & \eqref{xi}, \(\mathit{x} \notin FV(MN)\) \\ \hline
        & 3 & \(λx.Mx=M\) & - & \eqref{eta}, \(\mathit{x} \notin FV(M)\) \\ \hline
        & 4 & \(λx.Nx=N\) & - & \eqref{eta}, \(\mathit{x} \notin FV(N)\) \\ \hline
$\star$ & 5 & \(λx.Mx=N\) & 2,4 & \eqref{tau} \\ \hline
        & 6 & \(M=λx.Mx\) & 3 & \eqref{sigma} \\ \hline
        & 7 & \(M=N\) & 6,5 & \eqref{tau} \\ \hline

\end{tabular}
\end{center}
#+END_EXPORT
