| Sterne | Zeile | Formel  | Bezug | Regel |
|--------+-------+---------+-------+-------|
|        |     1 | $p$     | -     | Hyp   |
|        |     2 | $p \to q$ | -     | Hyp   |
|        |     3 | $p \lor q$ | 1     | $I\lor$  |
|        |     4 | $q$     | 1,3   | $E\to$  |
|        |     5 | $r$     | -     | Hyp   |
|        |     6 | $q \land r$ | 4,5   | $I\land$  |



| Sterne | Zeile | Formel  | Bezug | Regel |
|--------+-------+---------+-------+-------|
| +--   |     1 | $p$     | -     | hyp   |
| +--    |     2 | $p \lor q$ | 1     | $I\lor$  |
| -+-   |     3 | $p \to q$ | -     | hyp   |
| --+   |     4 | $r$     | -     | hyp   |
| ++- |     5 | $q$     | 1,3   | $E\to$  |
| +++  |     6 | $q \land r$ | 4,5   | $I\land$  |

- get first element of table (that is, first row)
- get the rule number, reference line and starstring
- get the properties of the proof rule
- check number of references → error if not correct
- function to do the right things with stars
- next row

(defun tryit ()
(tlt-nd-get-table)
(adjust-numbers)
(tlt-nd-correct-stars))


* TLT-ND-NEW does not work if there is just one hyp

(insert (format "%s" tlt-nd-table))

[[file:../../../../../media/storage/sciebo privat/org-mode/Bilder/2022-07-11_02-12-29_screenshot.png]]
*Overlays*:
org-table-add-rectangle-overlay
org-table-highlight-rectangle
before-change-functions → changes after each press

#+BEGIN_SRC elisp :exports code
  (defun tlt-nd-latex-strike-through (_strike-through contents info)
    "Transcode STRIKE-THROUGH from Org to LaTeX.
  CONTENTS is the text with strike-through markup.  INFO is a plist
  holding contextual information."
    (unless (search-forward-regexp "[\\+\\-]+$" contents nil t) ; exclude star strings from being stroke-through
      (org-latex--text-markup contents 'strike-through info)))

(defun org-latex-table-cell (table-cell contents info)
  "Transcode a TABLE-CELL element from Org to LaTeX.
CONTENTS is the cell contents.  INFO is a plist used as
a communication channel."
  (concat
   (let ((scientific-format (plist-get info :latex-table-scientific-notation)))
     (if (and contents
	      scientific-format
	      (string-match orgtbl-exp-regexp contents))
	 ;; Use appropriate format string for scientific
	 ;; notation.
	 (format scientific-format
		 (match-string 1 contents)
		 (match-string 2 contents))
       contents))
   (when (org-export-get-next-element table-cell info) " & ")))

(defun org-latex-table-cell (table-cell contents info)
  "Transcode a TABLE-CELL element from Org to LaTeX.
CONTENTS is the cell contents.  INFO is a plist used as
a communication channel."
  (concat
   (let ((scientific-format (plist-get info :latex-table-scientific-notation)))
     (if (and contents
	      scientific-format
	      (string-match orgtbl-exp-regexp contents))
	 ;; Use appropriate format string for scientific
	 ;; notation.
	 (format scientific-format
		 (match-string 1 contents)
		 (match-string 2 contents))
       contents))
   (when (org-export-get-next-element table-cell info) " & ")))
;;;; Table Row

(defun org-latex-table-row (table-row contents info)
  "Transcode a TABLE-ROW element from Org to LaTeX.
CONTENTS is the contents of the row.  INFO is a plist used as
a communication channel."
  (let* ((attr (org-export-read-attribute :attr_latex
					  (org-export-get-parent table-row)))
	 (booktabsp (if (plist-member attr :booktabs) (plist-get attr :booktabs)
		      (plist-get info :latex-tables-booktabs)))
	 (longtablep
	  (member (or (plist-get attr :environment)
		      (plist-get info :latex-default-table-environment))
		  '("longtable" "longtabu"))))
    (if (eq (org-element-property :type table-row) 'rule)
	(cond
	 ((not booktabsp) "\\hline")
	 ((not (org-export-get-previous-element table-row info)) "\\toprule")
	 ((not (org-export-get-next-element table-row info)) "\\bottomrule")
	 ((and longtablep
	       (org-export-table-row-ends-header-p
		(org-export-get-previous-element table-row info) info))
	  "")
	 (t "\\midrule"))
      (concat
       ;; When BOOKTABS are activated enforce top-rule even when no
       ;; hline was specifically marked.
       (and booktabsp (not (org-export-get-previous-element table-row info))
	    "\\toprule\n")
       contents "\\\\\n"
       (cond
	;; Special case for long tables.  Define header and footers.
	((and longtablep (org-export-table-row-ends-header-p table-row info))
	 (let ((columns (cdr (org-export-table-dimensions
			      (org-export-get-parent-table table-row) info))))
	   (format "%s
\\endfirsthead
\\multicolumn{%d}{l}{%s} \\\\
%s
%s \\\\\n
%s
\\endhead
%s\\multicolumn{%d}{r}{%s} \\\\
\\endfoot
\\endlastfoot"
		   (if booktabsp "\\midrule" "\\hline")
		   columns
		   (org-latex--translate "Continued from previous page" info)
		   (cond
		    ((not (org-export-table-row-starts-header-p table-row info))
		     "")
		    (booktabsp "\\toprule\n")
		    (t "\\hline\n"))
		   contents
		   (if booktabsp "\\midrule" "\\hline")
		   (if booktabsp "\\midrule" "\\hline")
		   columns
		   (org-latex--translate "Continued on next page" info))))
	;; When BOOKTABS are activated enforce bottom rule even when
	;; no hline was specifically marked.
	((and booktabsp (not (org-export-get-next-element table-row info)))
	 "\\bottomrule"))))))


  (org-export-define-backend 'tlt-nd-latex
    '((table-cell . org-latex-table-cell)
      (table-row . org-latex-table-row)
      (table . org-latex-table)
      (latex-fragment . org-latex-latex-fragment)
      (strike-through . tlt-nd-latex-strike-through)))

  (t (concat (org-latex--org-table table contents info)
             ;; When there are footnote references within the
             ;; table, insert their definition just after it.
             (org-latex--delayed-footnotes-definitions table info)))

  (defun tlt-export-natural-deduction ()
    (interactive)
    (org-export-to-buffer 'tlt-natural-deduction "*tlt-natural-deduction*"))
#+END_SRC
