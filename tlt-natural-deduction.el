(defcustom tlt-nd-stars '(1 "Sterne")
  "Number and name of the star column.")

(defcustom tlt-nd-rowcolumn '(2 "Zeile")
  "Number and name of the column to be numbered.")

(defcustom tlt-nd-formulas '(3 "Formel")
  "Number and name of the formula column.")

(defcustom tlt-nd-reference '(4 "Bezug")
  "Number and name of the reference column.")

(defcustom tlt-nd-rule '(5 "Regel")
  "Number and name of the rule column.")

(defcustom tlt-nd-hyp "Hyp"
  "Name of the assertion tlt-nd-rule.")

(defcustom tlt-nd-capitalize t
  "Whether to capitalize rule names automatically when aligning the table.")

(defcustom tlt-nd-no-ref-rules '(tlt-nd-no-stars tlt-nd-new-star)
  "Functions of rules which do not reference any line.")

(defcustom tlt-nd-write-hlines t
  "Whether or not to have horizontal lines between rows in LaTeX export.")

(defcustom tlt-nd-separate-header nil
  "Whether or not to separate the proof table's header
from the rest of the proof table in LaTeX export by
leaving a small space between the two objects.")

(defcustom tlt-nd-fix-alignment "|c|c|c|l|"
  "The alignment string to be used for every natural deduction proof table.
If `nil', take the ones taken from the ':align' argument.")

(defcustom tlt-nd-same-star-width t
  "Whether the star cells should all have the same width.
Takes labor from user by automatically adding LaTeX-code
to ensure this, but is repetitive. Relies on LaTeX's `array' package.")

(defcustom tlt-nd-skip-header nil
  "Whether or not to show the natural deduction proof header in LaTeX export.")

(defcustom tlt-nd-draw-frame t
  "Whether or not to draw the outer lines of the natural deduction table.")

(defcustom tlt-nd-rule-hyp '(:rule hyp
                                   :names ("Hyp" "$Hyp$" "Hypothesis")
                                   :refs 0
                                   :star-function tlt-nd-new-star)
  "Rule for introducing a hypothesis.")

(defcustom tlt-nd-rule-theorem '(:rule special-proven
                                       :names ("AL" "PC" "" "PL" "S1" "S2" "S3" "S4" "S5")
                                       :refs 2
                                       :star-function tlt-nd-no-stars)
  "Rule for introducing a formula already proved.")

(defcustom tlt-nd-rule-intro-and '(:rule intro-and
                                         :names ("$I\\land$" "I&")
                                         :refs 2
                                         :star-function tlt-nd-unite-from-reflist)
  "Rule for introducing a conjunction.")

(defcustom tlt-nd-rule-elim-and '(:rule elim-and
                                        :names ("$E\\land$" "E&")
                                        :refs 1
                                        :star-function tlt-nd-adopt-stars)
  "Rule for eliminating a conjunction.")

(defcustom tlt-nd-rule-intro-or '(:rule intro-or
                                        :names ("$I\\lor$" "Iv")
                                        :refs 1
                                        :star-function tlt-nd-adopt-stars)
  "Rule for introducing a disjunction.")

(defcustom tlt-nd-rule-elim-or '(:rule elim-or
                                       :names ("$E\\lor$" "Ev")
                                       :refs 3
                                       :star-function tlt-nd-unite-from-reflist)
  "Rule for eliminating a disjunction.")

(defcustom tlt-nd-rule-intro-if '(:rule intro-if
                                        :names ("$I\\to$" "I->")
                                        :refs 2
                                        :star-function tlt-nd-subtract-from-reflist)
  "Rule for eliminating a conditional.")

(defcustom tlt-nd-rule-elim-if '(:rule elim-if
                                       :names ("$E\\to$" "E->")
                                       :refs 2
                                       :star-function tlt-nd-unite-from-reflist)
  "Rule for eliminating a conditional.")

(defcustom tlt-nd-rule-elim-negation '(:rule elim-if-negation
                                             :names ("$E\\lnot$")
                                             :refs 2
                                             :star-function tlt-nd-unite-from-reflist)
  "Rule for introducing the bottom sign '⊥'.
This rule is considered an elimination rule by convention; intuitionists would not accept it anyway.")

(defcustom tlt-nd-rule-intro-negation '(:rule elim-if-negation
                                              :names ("$I\\lnot$" "$I-$" "I~")
                                              :refs 2
                                              :star-function tlt-nd-subtract-from-reflist)
  "Rule for introducing a negation.")

(defcustom tlt-nd-rule-elim-if-multi '(:rule elim-if-multi
                                             :names ("$I\\to_+$" "$I\\to_{+}$" "$I\\to^+$" "$I\\to^{+}$" "I->+")
                                             :refs n
                                             :star-function tlt-nd-subtract-from-reflist)
  "Rule for eliminating several hypotheses at once.")

(defcustom tlt-nd-proof-rules '(tlt-nd-rule-hyp
                                tlt-nd-rule-theorem
                                tlt-nd-rule-intro-and
                                tlt-nd-rule-elim-and
                                tlt-nd-rule-intro-or
                                tlt-nd-rule-elim-or
                                tlt-nd-rule-intro-if
                                tlt-nd-rule-elim-if
                                tlt-nd-rule-intro-negation
                                tlt-nd-rule-elim-negation
                                tlt-nd-rule-elim-if-multi)
  "List of rules to be considered for natural deduction proofs.")

(defvar tlt-nd-table nil
  "The table last fetched by `tlt-nd-get-table', possibly changed by functions like `tlt-nd-adjust-numbers' or `tlt-nd-apply-rules'.")

(defun nd-at-table-p ()
  (save-excursion
    (goto-char (org-table-begin))
    (let* ((header (tlt-nd-get-row))
           (stars (nth 1 tlt-nd-stars))
           (row (nth 1 tlt-nd-rowcolumn))
           (formula (nth 1 tlt-nd-formulas))
           (reference (nth 1 tlt-nd-reference))
           (rule (nth 1 tlt-nd-rule)))
      (when (equal header (list stars row formula reference rule)) t))))

(defun tlt-nd-capitalize-first-char (string)
  "Return STRING, first char capitalized.
        Return zero-length strings as-is."
  (if (equal string "") ""
    (concat (upcase (substring string 0 1)) (substring string 1))))

(defun tlt-nd-next-field-until (end)
  "From the position of point onwards until position END,
search for the next table field (which is possibly in the next row).
If it exists until END, go there and return `t'.
If it does not exist, return `nil'.
Great parts copied from `org-table-next-field'."
  (let ((org-table-tab-jumps-over-hlines t))
    (condition-case nil
        (progn
          (re-search-forward "|" end)                        ; go to the next occurrence of "|"
          (if (looking-at "[ \t]*$")                         ; if we are at the end of a line,
              (re-search-forward "|" end))                   ; go to the next occurrence of "|" (in the next line)
          (if (looking-at "-")                               ; if it is a horizontal line
              (re-search-forward "^[ \t]*|\\([^-]\\)" end t) ; go inside the first field of the next row;
            (goto-char (match-beginning 1))))                ; and move to the beginning of the field
      (error (if (equal end (point))
                 nil t)))))

(defun tlt-nd-next-field ()
  "Go to the next field. Return nil if at the end of the table."
  (tlt-nd-next-field-until (1- (org-table-end))))

(defun tlt-nd-get-row ()
  "Return a list of the contents of every cell in the row at hand.
    The strings are trimmed, capitalized and without properties.
      If `tlt-nd-capitalize' is non-nil, capitalize the first character of each column name."
  (let ((rowend (re-search-forward "|\s*$" nil t))
        (org-table-tab-jumps-over-hlines t)
        (search t)
        (row nil))
    ;; If not at the beginning of line, go there ;;
    (unless (bolp)
      (beginning-of-line))
    ;; move point right of the first occurrence of "|" ;;
    (re-search-forward "|")
    ;; get the field at hand and push it to `row'. Repeat until the end of the row ;;
    (while search
      (looking-at "[^|\r\n]*")

      (let* ((field-trim
              (org-trim                         ; trim whitespaces
               (buffer-substring-no-properties  ; and remove properties
                (match-beginning 0)             ; from the beginning of the field
                (match-end 0))))                ; to the end of the field

             (field
              ;; capitalize depending on custom variable ;;
              (if tlt-nd-capitalize
                  (tlt-nd-capitalize-first-char field-trim)
                field-trim)))

        (push field row))

      (setq search (tlt-nd-next-field-until rowend)))
    ;; return row ;;
    (reverse row)))

(defun tlt-nd-get-table ()
  "Store the table at hand in `tlt-nd-table'."
  (let ((end (org-table-end))
        (search t)
        (table nil))
    (save-excursion
      (goto-char (org-table-begin))
      ;; Loop
      (while search
        (push
         (tlt-nd-get-row)
         table)
        (setq search (tlt-nd-next-field)))
      ;; return table
      (setq tlt-nd-table (reverse table)))))

(defun tlt-nd-line (line)
  "Get list for line number LINE stored in `tlt-nd-table'.
Set the variable if it is nil."
  (if tlt-nd-table
      (nth (1- line) tlt-nd-table)
    (nth (1- line) (tlt-nd-get-table))))

(defun tlt-nd-row (row)
  "Get row number ROW from the table stored in `tlt-nd-table'.
     If there is no table stored, create it from the table at hand."
  (if tlt-nd-table
      (nth row tlt-nd-table) ; not -1 to ignore header
    (nth row (tlt-nd-get-table))))

(defun tlt-nd-column (colnum)
  "Get column number COLNUM."
  (unless tlt-nd-table (tlt-nd-get-table))
  (let ((counter 1)
        (length (length tlt-nd-table))
        (col nil))
    (while (<= counter length)
      (push
       (nth (1- colnum)
            (tlt-nd-line counter))
       col)
      (setq counter (1+ counter)))
    (reverse col)))

(defun tlt-nd-column-headless (colnum)
  "Get column number COLNUM without headlines."
  (cdr (tlt-nd-column colnum)))

(defun tlt-nd-field (colnum row)
  "Get field in column COLNUM and row ROW."
  (nth (1- colnum) (tlt-nd-row row)))

(defun tlt-nd-set-field (colnum row value)
  "Set field in column COLNUM and row ROW of `tlt-nd-table' to VALUE."
  (setf  (nth (1- colnum) (tlt-nd-row row)) value))

(defun replace-string-in-list (old new list)
  "Return the result of replacing OLD item by NEW item in LIST.

        Example:
        'old' 'new', '(1 'old' 2) → '(1, 'new' 2)"
  (let ((outlist))
    (dolist (element list outlist)
      (if (equal element old)
          (push new outlist)
        (push element outlist)))
    (reverse outlist)))

(defun order-list-string-int (list)
  "Return the result of ordering LIST by the numbers which are its members (stored as strings).

    Examples:
  '('3' '2' '1')             → '('1' '2' '3')
  '('2' '1' '5' '3' '2' '1') → '('1' '1' '2' '2' '3' '5')"
  (let* ((unquoted (mapcar #'string-to-number list)) ; important: (string-to-number "m3") returns 0!
         (sorted (sort unquoted #'<=)))
    (mapcar #'int-to-string sorted)))

(defun order-string (string)
  "RETURN the result of ordering the numbers occurring in STRING, which is of the form 'num_1, num_2, ..., num_n', as a string.
        Examples:
        '3,2,1'     → '1,2,3'
        '1,5,2,1,3' → '1,1,2,3,5'"
  (let* ((clean (gnus-strip-whitespace                     ; raw numstring
                 (substring-no-properties string)))                 ; with properties removed
         (list (split-string clean ","))
         (sorted (order-list-string-int list))
         (sorted-string (string-join sorted ",")))
    (if (equal sorted-string "0") "-"
      sorted-string)))

(defun tlt-nd-inspect-row (number-of-rows)
  (let* ((rownum 1)
         (row (tlt-nd-column-headless (car tlt-nd-rowcolumn)))
         (row-unstringed (mapcar #'string-to-number row)))
    (while (<= rownum number-of-rows)
      (let* ((actrownum (nth (1- rownum) row-unstringed))
             (occs-of-nr (count actrownum row-unstringed)))
        (when (> occs-of-nr 1)
          (error "\"%s\" occurs %d times in the row column, but must only occur once" actrownum occs-of-nr)))
      (setq rownum (1+ rownum)))))

(defun tlt-nd-correct-and-mark-refs (number-of-rows refcolnum wrongnum rightnum)
  "Replace every wrong reference number WRONGNUM in the reference column REFS, which
      is the REFCOLNUMth column in `tlt-nd-table', by the right reference number RIGHTNUM."
  (let ((rownumber 1)
        (tlt-nd-capitalize nil))
    (while (<= rownumber number-of-rows)
      (let* ((list-of-refs-table (tlt-nd-column-headless refcolnum))
             (string-of-refs-row (nth (1- rownumber) list-of-refs-table))    ; the nth field of the reference column, a string of reference numbers
             (list-of-refs-row (split-string string-of-refs-row ","))        ; list of references in a particular row, e.g. ("REF1" "REF2" "REF3")
             (rightnum-marked (concat "m" rightnum)))                        ; the right number marked with "m", e.g. "m2"

        (when (member wrongnum list-of-refs-row)
          ;; Now we replace our wrong number in the reference list with ;;
          ;; the marked right number and convert it back to a string    ;;
          (let* ((list-of-refs-row-corrected
                  (replace-string-in-list wrongnum rightnum-marked list-of-refs-row))     ; replace all occurrences of the wrongnum in list-of-refs-row by the marked rightnum
                 (string-of-refs-row-corrected (string-join list-of-refs-row-corrected ",")))       ; and make a string out of it

            (unless (or                                                        ; unless the ref field is empty (else the ref field would get the row number of the empty field)
                     (equal string-of-refs-row "")                             ; that is the empty string
                     (equal string-of-refs-row "-")                            ; or there is a hyphen in it: "-"
                     (equal string-of-refs-row "--"))                          ; or there is a double hyphen in it: "--"
              (tlt-nd-set-field refcolnum rownumber string-of-refs-row-corrected))))      ; put the new string back in the field
        (setq rownumber (1+ rownumber))))))

(defun tlt-nd-unmark-corrected-refs (number-of-rows ref)
  (let ((counter 2))
    (while (<= counter number-of-rows)
      (let* ((marked (tlt-nd-field ref counter))
             (unmarked (replace-regexp-in-string "m" "" marked)) ; also works for capital "M"
             (sorted (order-string unmarked))
             (forms (car tlt-nd-formulas)))

        (unless (equal (org-table-get counter forms) "")
          (tlt-nd-set-field ref counter sorted)))
      (setq counter (1+ counter)))))

(defun tlt-nd-adjust-numbers ()
  "Correct the row numbers in the table stored in `tlt-nd-table' and
     adjust the numbers in the row-column accordingly."
  (let* ((ref (car tlt-nd-reference))                                    ; 4 from (4 "Bezug")
         (row (car tlt-nd-rowcolumn))                                    ; 2 from (2 "Zeile")
         (number-of-rows (1- (length tlt-nd-table)))                     ; well, number of rows
         (org-table-allow-automatic-line-recalculation nil)              ; done with `ctrl-c-ctrl-c-hook' later anyway
         (org-table-automatic-realign nil)
         (refs (tlt-nd-column-headless ref))                             ; reference column
         (rownum 1))                                                     ; the correct row number (serves as counter, too!)

    ;; First of all, we make sure that no row
    ;; number occurs twice ;;
    (tlt-nd-inspect-row number-of-rows)
    ;; We start a while-loop to run the functions below for
    ;; every row. First of all, we get the string that is in the
    ;; row column, for example "3". Then we compare it with the
    ;; number that should be displayed, for example "2", in the
    ;; in the row. If they don't match, we set the row field
    ;; correctly. In this case, we would replace "3" by "2".

    (while (<= rownum number-of-rows)
      (let* ((rownum-str (int-to-string rownum))                         ; 1 → "1"
             (actnum (tlt-nd-field row rownum)))                         ; the actual (possibly wrong!) row number as a single-number string
        (unless (equal rownum-str actnum)                                ; if the correct rownumber rownum-str is not the actual rownumber actnum,
          (tlt-nd-set-field row rownum rownum-str)                       ; replace the actual (wrong!) row number with the right one

         ;;;             Getting the REFERENCE NUMBERS right             ;;

          ;; Now that we have spotted a wrong row number and corrected   ;;
          ;; it in the row number column, we also need to correct it in  ;;
          ;; the REFRENCE column. Thus, we loop over the reference       ;;
          ;; column and replace every occurrence of the wrong row number ;;
          ;; by the right row number marked with "m".                    ;;

          ;; The "m" makes sure that if this correct reference number    ;;
          ;; is at a wrong place in the row number column, our function  ;;
          ;; won't "miscorrect" our right number. We basically say       ;;
          ;; "this is the correct number, don't change it!"              ;;

          ;; To stick with our example, every occurrence of "3" in the     ;;
          ;; reference column becomes "m2"

          ;; Note that the unless-clause is not finished!                ;;

          (tlt-nd-correct-and-mark-refs number-of-rows ref actnum rownum-str))  ; wrongnum is the actual row number in string form (under the above condition)

        ;; Now we have updated the refernce fields with the correct number of   ;;
        ;; the FIRST row. So we repeat the while-loop to do this for EVERY row! ;;

        (setq rownum (1+ rownum))))

    ;; Now that every row number is correct and the row numbers in the reference column are adjusted, ;;
    ;; we remove our marks and sort the reference field so that the numbers are in the correct order  ;;

    (tlt-nd-unmark-corrected-refs number-of-rows ref)))

(defun tlt-nd-nth-char (n string)
  "Return Nth char in STRING. Nth is one-based."
  (substring string (1- n) n))

(defun delete-star (star str)
  "Replace '+' at position STAR in STR by '-'.
Assumes that at position STAR, there actually is a '+'."
  ;; replace the star by a "-"
  (replace-regexp-in-string  ; replace any "+" at level star-level-act
   (format "\\([\\+\\|\\-]\\{%d\\}\\)\\(\\+\\)\\([\\+\\|\\-]*\\)" (1- star)) ; 1- because we exclude them so the first char is the star level

   ;;  first group: all occurrences of "+" or "-" until, excluding, `right-star'
   ;;  second group: exactly one occurrence of "+" or "-": the one to replace  ;;
   ;;  third group: zero or more occurrences of "+" or "-"  - anything after              ;;
   "\\1-\\3" ; the wrong star is replaced by a "-"
   ;;  first group, second group replaced by ";%d;", third group          ;;
   str))

(defun add-star (no-star str)
  "Replace '-' at position NO-STAR in STR by '+'.
Assumes that at position NO-STAR, there actually is a '-'."
  (replace-regexp-in-string  ; replace any "+" at level star-level-act by ";star-amount;" if it exists
   (format "\\([\\+\\-]\\{%d\\}\\)\\([\\+\\|\\-]\\)\\([\\+\\|\\-]*\\)" (1- no-star)) ; 1- because we exclude them so the first char is the star level

   ;;  first group: all occurrences of "+" or "-" until, excluding, `no-star' - anything before
   ;;  second group: exactly one occurrence of "+" or "-": the one to replace  ;;
   ;;  third group: zero or more occurrences of "+" or "-"  - anything after              ;;
   "\\1+\\3" ; the wrong star is replaced by a "-"
   ;;  first group, second group replaced by ";%d;", third group          ;;
   str))

(defun ensure-no-star (pos str)
  "Ensure that at position POS in string STR, there is a '-', and return the resulting string."
  (if (not (equal (tlt-nd-nth-char pos str) "-"))
      (delete-star pos str)
    str))

(defun ensure-star (pos str)
  "Ensure that at position POS in string STR, there is a '+', and return the resulting string."
  (if (not (equal (tlt-nd-nth-char pos str) "+"))
      (add-star pos str)
    str))

(defun tlt-nd-count-hyps-until (rownumber)
  "Return the number of hypotheses in `tlt-nd-table' until (excluding) ROWNUMBER."
  (let ((hypcount 0)
        (counter 1))
    (while (< counter rownumber)
      (let* ((row (tlt-nd-row counter))
             (rulecol (1- (car tlt-nd-rule)))
             (rule (string-trim-left (nth rulecol row)))
             (hypnames (plist-get tlt-nd-rule-hyp :names)))
        (when (member rule hypnames)
          (setq hypcount (1+ hypcount)))
        (setq counter (1+ counter))))
    hypcount))

(defun tlt-nd-count-hyps ()
  "Return the number of hypotheses in `tlt-nd-table'."
  (tlt-nd-count-hyps-until (1- (length tlt-nd-table))))

(defun tlt-nd-fill-star (number-of-hyps star-string)
  "Fill the STAR-STRING with occurrences of '-' until the number of ocurrences of '+' or '-' is NUMBER-OF-HYPS.
        If NUMBER-OF-HYPS is the same length as STAR-STRING, just return the STAR-STRING.
  Examples:
  number-of-hyps 4, star-string '-+' → '-+--'.
  number-of-hyps 2, star-string '-++' → '-++'."
  (let* ((length (length star-string))
         (diff (- number-of-hyps length)))
    (if (> diff 0)
        (concat star-string
                (make-string diff ?-))
      star-string)))

(defun tlt-nd-fill-stars (number-of-hyps starlist)
  "Fill all star-strings which are element of STARLIST with occurrences of '-'
      until the number of occurrences of '+' or '-' is that of NUMBER-OF-HYPS.
     Return a list of the filled star-strings."
  (let ((filled-stars))
    (dolist (star-string starlist)
      (push (tlt-nd-fill-star number-of-hyps star-string) filled-stars))
    (reverse filled-stars)))

(defun tlt-nd-find-rule (rulestring)
  "Return that plist in `tlt-nd-proofrules' one of whose :names is RULESTRING, or an error message."
  (let ((right-rule)
        (rulenum (length tlt-nd-proof-rules))
        (counter 1))
    (while (<= counter rulenum)
      (let* ((rulevar (nth (1- counter) tlt-nd-proof-rules))
             (rule (eval rulevar)) ; eval necessary because rulevar is just a sign
             (rulenames (plist-get rule :names)))
        (if (member rulestring rulenames)
            (progn
              (setq right-rule rule)
              (setq counter (1+ rulenum))) ; set the counter so high that the while-loop stops
          (setq counter (1+ counter)))))
    (if right-rule right-rule
      (error "No rule called \"%s\" within tlt-nd-proof-rules" rulestring))))

(defun tlt-nd-get-stars-from-refs (starcol refnums)
  "Return a list of the starstrings in the rows in REFNUMS,
where STARCOL is the number of the column in which the stars are.
STARCOL is an integer, REFNUMS a list of integers."
  (let ((hyplist))
    (dolist (ref refnums)
      (push (tlt-nd-field starcol ref) hyplist))
    (reverse hyplist)))

(defun tlt-nd-no-stars (current-row star-column number-of-hyps)
  "Set the starfield in current-row CURRENT-ROW and column STAR-COLUMN to be a string of NUMBER-OF-HYPS occurrences of '-'.
CURRENT-ROW, STAR-COLUMN and NUMBER-OF-HYPS are integers.

    Meant to be used for deduction rules which do not need a hypothesis."
  (let ((stars-right (make-string number-of-hyps ?-)))
    (tlt-nd-set-field star-column current-row stars-right)
    stars-right))

(defun tlt-nd-new-star (star-column current-row number-of-hyps hyps-until-current-row)
  "Set the starfield in current-row CURRENT-ROW and column STAR-COLUMN to a string of NUMBER-OF-HYPS characters, consisting of occurrences of '-', except for position NUMBER-OF-HYPS, which is '+'.
CURRENT-ROW, STAR-COLUMN, HYPS-UNTIL-CURRENT-ROW and NUMBER-OF-HYPS are integers."
  (let ((stars-right  (tlt-nd-fill-star number-of-hyps (format "%s+" (make-string hyps-until-current-row ?-)))))
    (tlt-nd-set-field star-column current-row stars-right)
    stars-right))

(defun tlt-nd-adopt-stars (star-column current-row reference-list)
  "Set the star field located in the STAR-COLUMN of the CURRENT-ROW to be the same as the first item in REFERENCE-LIST.
CURRENT-ROW and STAR-COLUMN are integers, REFERENCE-LIST is a list of integers."
  (let ((right-star (tlt-nd-field star-column (car reference-list))))
    (tlt-nd-set-field star-column current-row right-star)
    right-star))

(defun tlt-nd-unite (number-of-hyps hyplist)
  "Return the union of all hypotheses in HYPLIST, based on the NUMBER-OF-HYPS.
          Assumes that all hypotheses in HYPLIST have the same length, whitespace removed."
  (let* ((right-star  (make-string number-of-hyps ?-))) ; e.g. " +++"
    (dolist (star-string hyplist)
      (let ((position 1))
        (while (<= position (length star-string))
          (let* ((str (tlt-nd-nth-char position star-string)))
            (when (equal str "+")
              (setq right-star
                    (ensure-star position right-star))))
          (setq position (1+ position)))))
    right-star))

(defun tlt-nd-unite-from-reflist (star-column current-row number-of-hyps reference-list)
  "Set the string in STAR-COLUMN and CURRENT-ROW such that it is the
          intersection of all hypotheses to which REFERENCE-LIST points.
          Assumes that all hypotheses have the same length."
  (let* ((hyplist (tlt-nd-fill-stars number-of-hyps (tlt-nd-get-stars-from-refs star-column reference-list))) ; e.g. ("+--" "++-")
         (union (tlt-nd-unite number-of-hyps hyplist)))
    (tlt-nd-set-field star-column current-row union)))

(defun subtract-from-star (minuend subtrahend)
  "Return the result of calculating the difference between MINUEND and SUBTRAHEND.

With 'calculating the difference', the following is meant:

      + AND + → -
      + AND - → +
      - AND + → +
      - AND - → -

Works like an exclusive OR-statement (if + is interpreted as t and - as nil). Thus, to be exact, there is no minuend or substrahend, since the function is commutative."
  (let ((difference minuend)
        (position 1))
    (while (<= position (length minuend))
      (let* ((symbol_minuend (tlt-nd-nth-char position minuend))
             (symbol_subtrahend (tlt-nd-nth-char position subtrahend)))
        (cond ((and (equal symbol_minuend "+") (equal symbol_subtrahend "+")) ; both +
               (setq difference (ensure-no-star position difference)))
              ((or (and (equal symbol_minuend "+") (equal symbol_subtrahend "-"))
                   (and (equal symbol_minuend "-") (equal symbol_subtrahend "+")))
               (setq difference (ensure-star position difference)))
              ((and (equal symbol_minuend "-") (equal symbol_subtrahend "-"))
               (setq difference (ensure-no-star position difference))))
        (setq position (1+ position))))
    difference))

(defun tlt-nd-subtract (hyplist)
  "Return the result of successively subtracting the cdr of HYPLIST from the car of HYPLIST."
  (let ((subtrahends (cdr hyplist))
        (difference (car hyplist)))
    (dolist (subs subtrahends)
      (setq difference (subtract-from-star difference subs)))
    difference))

(defun tlt-nd-subtract-from-reflist (star-column current-row number-of-hyps reference-list)
  "Set the string in STAR-COLUMN and CURRENT-ROW such that it is the
          intersection of all hypotheses to which REFERENCE-LIST points.
          Assumes that all hypotheses have the same length."
  (let* ((hyplist (tlt-nd-fill-stars number-of-hyps (tlt-nd-get-stars-from-refs star-column reference-list))) ; e.g. ("+--" "++-")
         (difference (tlt-nd-subtract hyplist)))
    (tlt-nd-set-field star-column current-row difference)))

(defun tlt-nd-apply-rules ()
  "Apply rules."
  (let ((rowstotal (1- (length tlt-nd-table))) ; total number of rows
        (current-row 1)                        ; row-counter
        (rule-column (car tlt-nd-rule))        ; in which column the rule column is
        (ref-column (car tlt-nd-reference))    ; in which column the reference column is
        (star-column (car tlt-nd-stars))       ; in which column the star column is
        (number-of-hyps (tlt-nd-count-hyps)))  ; the number of hypotheses

    (while (<= current-row rowstotal)      ; as long as we are at one of the proof's rows,
      (let* ((rule-string (tlt-nd-field rule-column current-row)) ; get name of rule from rule-string
             (proofrule (tlt-nd-find-rule rule-string))           ; find property list of rule with information
             (hyps-until-current-row (tlt-nd-count-hyps-until current-row)))

        ;; If our star function is NOT dependent on hypotheses, set  ;;
        ;; the reference column to be empty and set the hypothesis   ;;
        ;; field to as many minuses as there are hypotheses          ;;

        ;; If our star function IS dependent on hypotheses, get the
        ;; reference field of our row, extract the references, and
        ;; check whether the number of references matches the number
        ;; of references that our proof rule expects. If not, return
        ;; an error.

        (let* ((reference-string (gnus-strip-whitespace (tlt-nd-field ref-column current-row))) ; reference-string
               (reference-list-raw (if (equal reference-string "-") nil (split-string reference-string ","))) ; return nil if our reference list is empty so that the condition below goes through
               (reference-list (mapcar #'string-to-number reference-list-raw))
               (number-of-refs (plist-get proofrule :refs)))                 ; reference list with actual numbers, e.g. '(1 2)

          (if (not (or (equal number-of-refs 'n)
                       (equal (length reference-list) number-of-refs)))
              (error "Proofrule %s needs %d references, but %d provided!"
                     rule-string (plist-get proofrule :refs) (length reference-list))

            ;; If the number of references is correct, though, find the
            ;; star-function's arguments and apply the rule ;;

            (let* ((star-function (plist-get proofrule :star-function))
                   (argument-list (mapcar 'symbol-value (help-function-arglist star-function))))
              (apply star-function argument-list)))))

      (setq current-row (1+ current-row)))))

(defun tlt-nd-make-linestring (line)
  "Return the table string of LINE. If LINE is 1, add an h-line-string."
  (if (= line 1)
      (concat "| "
              (string-join (tlt-nd-line line) "|") "|\n|---")
    (concat "| "
            (string-join (tlt-nd-line line) "|") "|")))

(defun tlt-nd-make-table ()
  "Create a table from the data stored in `tlt-nd-table'."
  (let ((line 1)
        (length (length tlt-nd-table))
        (table ""))
    (while (<= line length)
      (setq table
            (concat table (tlt-nd-make-linestring line) "\n"))
      (setq line (1+ line)))
    table))

(defun tlt-nd-insert-table ()
  "Insert the table stored in `tlt-nd-table' at point."
  (save-excursion
    (insert (tlt-nd-make-table))
    (org-table-align)))

(defun tlt-nd-replace-table ()
  "Delete the table at point and replace it with the table stored in `tlt-nd-table'."
  (let ((marker (point))
        (beg (org-table-begin))
        (end (org-table-end)))
    (delete-region beg end)
    (tlt-nd-insert-table)
    (goto-char marker)
    (re-search-forward "[^|]*" )
    (skip-chars-backward "\s\t"))
  (message "Table updated"))

(defun tlt-nd-count-hyps-exported ()
  (goto-char (point-min))
  (re-search-forward "^[\\+\\-]+\s*\\&" nil t)
  ;; return its coordinates
  (let* ((beg (match-beginning 0))
         (end (match-end 0)))
    (- end beg 2))) ; -2 to disregard " &" at the end

(defun tlt-nd-insert-stars (num)
  "Convert the star-string consisting of pluses and minuses
      to a LaTeX-compatible one. For example,
    +++ → $\star$ & $\star$ & $\star$ &
    ++- → $\star$ & $\star$ &         &
    +-+ → $\star$ &         & $\star$ &
    +-- → $\star$ &         &         &"
  (goto-char (point-min))
  ;; go to every star-string
  (while (re-search-forward "^[\\+\\-]+\s*\\&" nil t)
    ;; return its coordinates
    (let ((beg (match-beginning 0))
          (end (match-end 0)))
      ;; narrow the buffer to the star-string using the coordinates
      (narrow-to-region beg end)
      (goto-char (point-max))
      ;; if an ampersand follows an occurrence of '+', make the
      ;; '+' a star, but do not add an extra ampersand
      (or ;; --
       (when (re-search-backward "\\-\\-\s*\\&" nil t)
         (replace-match "         &         &"))
       ;; +-
       (when (re-search-backward "\\+\\-\s*\\&" nil t)
         (replace-match " $\\\\star$ &         &"))
       ;; ++
       (when (re-search-backward "\\+\\+\s*\\&" nil t)
         (replace-match " $\\\\star$ & $\\\\star$ &"))
       ;; -+
       (when (re-search-backward "\\-\\+\s*\\&" nil t)
         (replace-match "         & $\\\\star$ &")))
      ;; after that, replace every '+' by a star and an extra ampersand
      (goto-char (point-min))
      (if (= num 1)
          (replace-regexp "+" "$\\\\star$")
        (replace-regexp "+" "$\\\\star$ &"))
      (goto-char (point-min))
      (if (= num 1)
          (replace-regexp "-" "       ") ; whitespaces to align with "\star"
        (replace-regexp "-" "        &")) ; whitespaces to align with "\star"
      ;; and don't forget to widen the buffer again!
      (widen))))

(defun tlt-natded-org-latex-strike-through (fun _strike-through contents info)
  "If CONTENTS is a string consisting only of occurrencces of '+' or '-',
create the necessary star-string. Else, strike-through CONTENTS."
  (let ((star-string-p
         (with-temp-buffer
           (insert (format "%s" contents))
           (goto-char (point-min))
           (re-search-forward "\t*[\\+\\|\\-]+\t*$" nil t))))
    (if star-string-p (concat "+" contents "+")
      (funcall fun _strike-through contents info))))

(advice-add #'org-latex-strike-through :around #'tlt-natded-org-latex-strike-through)

(defun tlt-nd-remove-header ()
  "Remove the header from the LaTeX table at point."
  (save-excursion
    (goto-char (point-min))
    (re-search-forward (return-header-latex))
    (beginning-of-line)
    (delete-region (point) (progn (forward-line) (point)))
    (when (and (not tlt-nd-draw-frame) (looking-at "\\\\hline"))
      (replace-match "")
      (backward-char))))

(defun tlt-nd-insert-multicolumn (num)
  "Wrap the header of the first column in a `multicolumn'-command.
      NUM is the number of columns. For example, `(tlt-nd-insert-multicolumn 3)'
      yields '\\multicolumn{3}{|c|}{Sterne}' if 'Sterne' is the column's header."
  (save-excursion
    (goto-char (point-min))
    (re-search-forward (return-header-latex))
    (beginning-of-line)
    (insert (format "\\multicolumn{%d}{|c|}{" num))
    (forward-word)
    (insert "}")))

(defun tlt-nd-handle-hlines ()
  "If `tlt-natded-write-hlines' is non-nil, make sure all rows
      are separated by horizontal lines. Further, if `tlt-nd-separate-header'
      is non-nil, separate the header from the proof table by leaving
      a small space between the two rows."
  (replace-regexp "\n\\\\hline" "")
  (when tlt-nd-write-hlines
    (goto-char (point-min))
    (search-forward-regexp "\\\\begin{natded}{[^}]*}") ; "natded" replaced by "tabular" already
    (unless tlt-nd-skip-header (insert "\n\\hline")) ; for top horizontal line of header
    (replace-regexp "\\\\\\\\$" " \\\\\\\\ \\\\hline")
    (when tlt-nd-separate-header
      (goto-char (point-min))
      (let ((beg (re-search-forward (concat "^" (return-header-latex))))
            (end (re-search-forward "$")))
        (replace-regexp "\\\\hline" "" nil beg end)))
    (unless tlt-nd-draw-frame
      (goto-char (point-max))
      (re-search-backward "\\\\hline" nil t)
      (replace-match ""))))

(defun build-alignment-string (num)
  "Build the string to be inserted in '\\begin{tabular}{…}'."
  (let ((result "")
        (counter 1)
        (string (if tlt-nd-same-star-width
                    (format ">{\\centering\\arraybackslash}p{\\dimexpr\\wd1/%d}|" num)
                  "c|")))
    (while (<= counter num)
      (setq result (concat result string)) ; one "c" per hypothesis
      (setq counter (1+ counter)))
    (when tlt-nd-draw-frame
      (setq result (concat "|" result))) ; "|" at the beginning
    (setq result (substring result 0 (- (length result) 1))))) ; stripping last "|" off to avoid doubling

(defun tlt-nd-adjust-alignment (num)
  "Change the LaTeX table at point from a natded to a
tabular environment and, depending on the value of
`tlt-nd-same-star-width', make all star cells the same size."
  (save-excursion
    (goto-char (point-min))
    (if tlt-nd-same-star-width
        ;; if stars cells should be automatically adjusted ;;
        (progn
          (replace-regexp "^\\\\begin{natded}{" (format "\\\\setbox1=\\\\hbox{%s}\n\\\\begin{tabular}{" (nth 1 tlt-nd-stars) num))
          (insert (build-alignment-string num))
          (replace-regexp "^\\\\end{natded}" "\\\\end{tabular}"))
      ;; if they shouldn't be automatically adjusted ;;
      (replace-regexp "^\\\\begin{natded}{" "\\\\begin{tabular}{")
      (insert (build-alignment-string num))
      (replace-regexp "^\\\\end{natded}" "\\\\end{tabular}"))))

(defvar latex-column-regexp "[^&\n]*&"
  "Regular expression matching the next column
in a LaTeX table.")

(defun tlt-nd-tex-colno-regexp (colno)
  "Return regexp to match column COLNO in a LaTeX table."
  (let* ((counter 1)
         (regexp "^"))
    (while (< counter colno)
      (setq regexp (concat regexp latex-column-regexp))
      (setq counter (1+ counter)))
    regexp))

(defun reference-column-regexp (number-of-hyps)
  "Return regexp to match the reference column.
NUMBER-OF-HYPS is needed to skip the star columns."
  (let* ((starcolno (car tlt-nd-stars))
         (refcolno (car tlt-nd-reference))
         (diff (- refcolno starcolno))) ; equals 3 usually
    ;; if the reference column comes AFTER the star column,
    ;; find the starcolumn, skip it, and go to the ref column
    (if (wholenump diff)
        (let ((reference-colno-multi (+ starcolno number-of-hyps (1- diff))))
          (tlt-nd-tex-colno-regexp reference-colno-multi))
      ;; if the reference column comes BEFORE the star column, just go there
      ;; since there is nothing-to-skip
      (tlt-nd-tex-colno-regexp refcolno))))

(defun tlt-nd-adjust-empty-references (number-of-hyps)
  "Replace every hyphen in the reference column by
a double hyphen. NUMBER-OF-HYPS is needed to find the
reference column."
  (let ((regexp (reference-column-regexp number-of-hyps)))
    (goto-char (point-min))                         ; go to the beginning of the buffer
    (while (re-search-forward regexp nil t)         ; search for reference column
      (when (re-search-forward "[^&\n]*&" nil t)    ; search for next column
        (let ((beg (match-beginning 0))             ; beginn ng of reference column
              (end (match-end 0)))                  ; end of reference column
          (replace-regexp " - " " -- " nil beg end)))))) ; replace "-" by "--" in reference column

(defun prepare-nd-table ()
  "Convert the contents of a LaTeX table to a natural deduction table."
  (let ((num (tlt-nd-count-hyps-exported)))
    (tlt-nd-handle-hlines)
    (cond (tlt-nd-skip-header (tlt-nd-remove-header))
          ((> num 1) (tlt-nd-insert-multicolumn num)))
    (tlt-nd-insert-stars num)
    (tlt-nd-adjust-alignment num)
    (tlt-nd-adjust-empty-references num)))

(defun tlt-nd-frame-alignment ()
  "Return the alignment string for the proof table, or `nil', if tlt-nd-fix-alignment is `nil'.
Also respect `tlt-nd-draw-frame'."
  (if tlt-nd-draw-frame
      tlt-nd-fix-alignment
    (let ((first-char (substring tlt-nd-fix-alignment 0 1))
          (last-char (substring tlt-nd-fix-alignment
                                (1- (length tlt-nd-fix-alignment))
                                (length tlt-nd-fix-alignment))))
      (cond ((and (equal first-char "|")
                  (equal last-char "|"))
             (substring tlt-nd-fix-alignment 0 (1- (length tlt-nd-fix-alignment))))
            ((equal first-char "|") (substring tlt-nd-fix-alignment 1 (length tlt-nd-fix-alignment)))
            ((equal last-char "|") (substring tlt-nd-fix-alignment 0 (1- (length tlt-nd-fix-alignment))))
            (t tlt-nd-fix-alignment)))))

(defun natded-export (fun table contents info)
  "Return appropriate LaTeX code for a natural deduction table.

      TABLE is the table type element to transcode.  CONTENTS is its
      contents, as a string.  INFO is a plist used as a communication
      channel."
  (let* ((attr (org-export-read-attribute :attr_latex table))
         (environment (plist-get attr :environment)))
    (if (equal environment "natded")
        ;; If we have a natural deduction environment,
        ;; change it accordingly
        (progn
          (let* ((alignment (or (tlt-nd-frame-alignment) (org-latex--align-string table info)))
                 (caption (org-latex--caption/label-string table info))
                 (above? (org-latex--caption-above-p table info))
                 (output (format "\\begin{natded}{%s}\n%s\n\\end{natded}"
                                 alignment
                                 contents))
                 (natded-table
                  (with-temp-buffer
                    (insert output)
                    (goto-char (point-min))
                    (prepare-nd-table)
                    (buffer-substring-no-properties (point-min) (point-max)))))
            (org-latex--decorate-table natded-table attr caption above? info)))
      ;; Else just call the function
      (funcall fun table contents info))))

(advice-add #'org-latex--org-table :around #'natded-export)

(defun return-header-latex ()
  "Return the first row of the LaTeX `table' environment at point."
  (let* ((stars (nth 1 tlt-nd-stars))
         (row (nth 1 tlt-nd-rowcolumn))
         (formula (nth 1 tlt-nd-formulas))
         (reference (nth 1 tlt-nd-reference))
         (rule (nth 1 tlt-nd-rule))
         (amp " & "))
    (concat stars amp row amp formula amp reference amp rule)))

(defun nd-latex-table-p ()
  "Return non-nil if at latex-table."
  (when (re-search-forward (nth 1 tlt-nd-stars) nil t)
    (beginning-of-line)
    (when (re-search-forward (return-header-latex) nil t) t)))

(provide 'tlt-natural-deduction)
